var {Pool, Client} = require('pg');
var pool = new Pool({
    connectionString: process.env.DATABASE_URL
});

var Codes = {
  getSectorList : function(callBackFunc) {
    var sectorListQuery = 'select code_id as code, code_name from code_mst where code_pid = \'sector\' order by order_no';

    var context = {};
    pool.query(sectorListQuery, (err,results) => {
      if (err || results.rowCount === 0) {
        context = null;
        throw err;
      } else {
        context = results.rows;
      }
      callBackFunc(context);
    })
  },
  getMajorList : function(callBackFunc) {
    var majorListQuery = 'select code_id as code, code_name from code_mst where code_pid = \'major\' order by order_no';

    var context = {};
    pool.query(majorListQuery, (err,results) => {
      if (err || results.rowCount === 0) {
        context = null;
        throw err;
      } else {
        context = results.rows;
      }
      callBackFunc(context);
    })
  },
  getUserTypeList : function(callBackFunc) {
    var userTypeListQuery = 'select code_id as code, code_name from code_mst where code_pid = \'user.type\' order by order_no';

    var context = {};
    pool.query(userTypeListQuery, (err,results) => {
      if (err || results.rowCount === 0) {
        context = null;
        throw err;
      } else {
        context = results.rows;
      }
      callBackFunc(context);
    })
  },
  getUserSexList : function(callBackFunc) {
    var userSexListQuery = 'select code_id as code, code_name from code_mst where code_pid = \'user.sex\' order by order_no';

    var context = {};
    pool.query(userSexListQuery, (err,results) => {
      if (err || results.rowCount === 0) {
        context = null;
        throw err;
      } else {
        context = results.rows;
      }
      callBackFunc(context);
    })
  }
}
module.exports = Codes;