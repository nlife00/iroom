/**
 * Main
 * 
 * @date 2019-01-17
 * @author hcjung
 */

require('dotenv').config();

// express
var express = require('express')
  , path = require('path');

// express middleware
var bodyParser = require('body-parser')
  , cookieParser = require('cookie-parser')
  , static = require('serve-static')
  , errorHandler = require('errorhandler');

// error handler
var expressErrorHandler = require('express-error-handler');

// session middleware
var expressSession = require('express-session');
var FileStore = require('session-file-store')(expressSession);

// custom module    
var iroom = require('./lib/util');

// express object
var app = express();

// view engine
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

// application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extend: true}));
// application/json
app.use(bodyParser.json());

// public folder open
app.use(express.static(path.join(__dirname, 'public')));

// cookie-parser
app.use(cookieParser());

// session configuration
app.use(expressSession({
  secret : 'saper',
  resave : false,
  saveUninitialized : true
  //store : new FileStore({logFn: function(){}})
}));
  
var passport = require('./lib/passport')(app);  

app.use(passport.initialize());
app.use(passport.session());

// session info
app.use(function(req,res,next) {
  res.locals.user = req.user;
  next();
})

// route
var querys = require('./routes/ajaxRouter');
var users = require('./routes/userRouter');
var projects = require('./routes/projectRouter');
var menuRouter = require('./routes/menuRouter');
var authRouter = require('./routes/authRouter')(passport);

//loginProc.init();
app.use('/users',users);
app.use('/projects',projects);
app.use('/querys',querys);
app.use('/menu',menuRouter);
app.use('/',authRouter);

// Error Handler Setting
var errorHandler = expressErrorHandler({
  static: {
    '404': './public/html/404.html'
  }
});

app.use(function (req, res, next) {
  res.status(404).send('Sorry cant find that!');
});

app.use(function (err, req, res, next) {
  console.error(err.stack)
  res.status(500).send('Something broke!')
});

app.use( expressErrorHandler.httpError(404) );
app.use( errorHandler );

module.exports = app;