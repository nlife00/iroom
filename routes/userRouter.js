var express = require('express');
var router = express.Router();
var util = require('../lib/util');
var userQueries = require('../db/user.db');
var codeQueries = require('../db/codes.db');
var {Pool, Client} = require('pg');
var pool = new Pool({
    connectionString: process.env.DATABASE_URL
});

// User 전체화면 - userContents
router.get('/', function(req,res) {
  res.render('users/userContents');
})

// userSearch
router.get('/userSearch', function(req,res) {
  res.render('users/userSearch');
})

// userList
router.get('/userList', function(req,res) {
  res.render('users/userList');
})

// User 목록 데이터
router.get('/users', function(req,res) {
  var param = {
    sector : req.query.sector,
    major : req.query.major,
    userType : req.query.userType,
    userName : req.query.userName
  }
  userQueries.getUserList(param, (results) => {
    var list = "";
    var user = req.user;
    if (results.rowCount !== 0) {
      results.forEach(users => {
        list = list + "<tr>" +
          "<td><a href=\"javascript:userModal('" + users.user_id + "')\">" + users.user_name + "</a></td>" +
          "<td>" + users.sector + "</td>" +
          "<td>" + users.major1 + "</td>" +
          "<td>" + users.user_type + "</td><td>";
          if (user.user_level < 2) {
            list = list + "<a href=\"javascript:userProject('" + users.user_id + "') \"><img src=\"/img/modify.png\" width=\"10\" height=\"10\" /></a> ";
          };

          list = list + users.project_name + "</td>" +
          "<td>" + users.end_date + "</td>" +
          "<td>" + users.phone_no + "</td>" +
          "<td>" + users.email + "</td>" +
          "<td>" + users.home_address + "</td>" +
          "<td>" + users.major2 + "</td>" +
          "<td>" + users.birthday + "</td>" +
          "<td>" + users.sex + "</td>" +
          "</tr>";
      });
    } else {
      list = list + "<tr><td colspan='13' align='center'>데이터가 없습니다.</td></tr>";
    }
    res.send(list);
  })
})

// GET - User 정보
router.get('/user/:userId', function(req,res) {
  var user = req.user;
  if (user.user_level < 2) {
    if (req.params.userId != 0) {  // 기존 사용자 정보 수정
      userQueries.getUser(req.params.userId, (results) => {
        res.render('users/userEdit',results);
      })
    } else { // 신규 사용자 등록
      res.render('users/userNew');
    }
  } else {  // 수정권한이 없는 경우
    userQueries.getUser(req.params.userId, (results) => {
      res.render('users/userView',results);
    })
  }
})

// 인재 프로젝트 리스트
router.get('/project/:userId', function(req,res) {
  var user = req.user;
  userQueries.getUserProject(req.params.userId, (results) => {
    res.render('users/userProject',results);
  })
})

// POST - User proejct Info 
router.post('/project', function(req,res) {
  userQueries.setUserProject(req.body, (results) => {
    res.send();
  })
})

// DELETE - User proejct Info 
router.delete('/project/:user_project_seq', function(req,res) {
  userQueries.deleteUserProject(req.params.user_project_seq, (results) => {
    res.send();
  })
})

// PUT - User 정보 
router.put('/user', function(req,res) {
  userQueries.setUser(req.body, (results) => {
    res.send();
  })
})

// POST - User 정보
router.post('/user', function(req,res) {
  userQueries.newUser(req.body, (results) => {
    res.send();
  })
})

module.exports = router;