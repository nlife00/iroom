module.exports = function(app) {
  var user = require('../db/user.db');

  // passport
  var passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;

  passport.serializeUser((user, done) => { 
    //console.log('serialize', user);
    done(null, user); 
  });

  passport.deserializeUser((user, done) => {
    //console.log('deserialize',user);
    done(null, user);
  });

  passport.use(new LocalStrategy(
    {
      usernameField: 'id',
      passwordField: 'password',
      passReqToCallback : true
    },
    function(req, username, password, done) {       
      if (req.body.userType === 'C') {
        user.findCompanyUser({id:username, pwd:password}, function(err,user) {
          if (err) { return done(err); }
          if (user) {
            console.log('Login Success', user);
            return done(null, user, 'welcome');
          } else {
            return done(null, false, 'login failed');
          }
        }); 
      } else {
        user.findUser({id:username, pwd:password}, function(err,user) {
          if (err) { return done(err); }
          if (user) {
            console.log('Login Success', user);
            return done(null, user, 'welcome');
          } else {
            return done(null, false, 'login failed');
          }
        }); 
      }
    } 
  ));

  return passport;
}