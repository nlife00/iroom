var {Pool, Client} = require('pg');
var pool = new Pool({
    connectionString: process.env.DATABASE_URL
});

var Menu = {
  getMenu : function(user, callBackFunc) {
    var getMenuQuery = 'select menu_user_map.menu_id, menu_user_map.menu_name, menu_mst.menu_link from menu_user_map'
                      + ' inner join menu_mst on menu_mst.menu_id = menu_user_map.menu_id'
                      + ' where category = \'' + user.category + '\''
                      + ' and user_level = \'' + user.user_level + '\''
                      + ' order by menu_order';

    var context = {};
    pool.query(getMenuQuery, (err,results) => {
      if (err || results.rowCount === 0) {
        context = null;
        throw err;
      } else {
        context = {menu : results.rows};
      }
      callBackFunc(context);
    })
  }
}
module.exports = Menu;