var express = require('express');
var router = express.Router();
var {Pool, Client} = require('pg');
var pool = new Pool({
  connectionString: process.env.DATABASE_URL
});
var util = require('../lib/util');
var projectQueries = require('../db/project.db');

router.get('/', function(req,res) {
  res.render('projects/projectList');
})

router.get('/projects', function(req,res) {
  var context = {};
  projectQueries.getProjects(req.query, (results) => {
    context = results.rows;
    res.send(context);
  })
})

router.get('/project/:projectNo', function(req,res) {
  var user = req.user;
  if ((user.user_level < 2 && user.category === 'U') || (user.category === 'C')) {
    if (req.params.projectNo != 0) {  // 기존 프로젝트 정보 수정
      projectQueries.getProject(req.params, (results) => {
        res.render('projects/projectEdit',results);
      })
    } else { // 신규 프로젝트 등록
      res.render('projects/projectNew');
    }
  } else {  // 수정권한이 없는 경우
    projectQueries.getProject(req.params, (results) => {
      res.render('projects/projectView',results);
    })
  }
})

router.put('/project', function(req,res) {
  projectQueries.setProject(req.body, (results) => {
    res.send();
  })
})

router.post('/project', function(req,res) {
  projectQueries.newProject(req.body, (results) => {
    res.send();
  })
})

module.exports = router;