var express = require('express');
var router = express.Router();
var codeQueries = require('../db/codes.db');

router.get('/sectorList', function(req,res) {
  codeQueries.getSectorList((results) => {
    res.send(results);
  })
})

router.get('/majorList', function(req,res) {
  codeQueries.getMajorList((results) => {
    res.send(results);
  })
})

router.get('/userTypeList', function(req,res) {
  codeQueries.getUserTypeList((results) => {
    res.send(results);
  })
})

router.get('/userSexList', function(req,res) {
  codeQueries.getUserSexList((results) => {
    res.send(results);
  })
})

module.exports = router;