var express = require('express');
var router = express.Router();
    
router.get('/', function(req, res) {
  var menu = require('../db/menu.db');

  menu.getMenu(req.user, (results) => {
    results.user_name = req.user.user_name;
    res.render('common/menu', results);
  })
});

module.exports = router;