var {Pool, Client} = require('pg');
var pool = new Pool({
    connectionString: process.env.DATABASE_URL
});
var util = require('../lib/util');

var User = {
  findUser : function(userData, done) {
    var findUserQuery = 'select user_id, user_name, user_level, code_name from user_mst '
                      + 'left join code_mst on code_id = \'U\' and code_pid = \'init.page\' '
                      + 'where 1=1 '
                      + 'and user_id = \'' + userData.id + '\' '
                      + 'and password = \'' + userData.pwd + '\''

    //console.log(findUserQuery);
    var context = {};
    pool.query(findUserQuery, (err,results) => {
      if (err) { done(err); return false; }
      if (results.rowCount === 0) {
        userData = null;
      } else {
        context = results.rows[0];
        userData.id = context.user_id;
        userData.user_name = context.user_name;
        userData.category = 'U';
        userData.user_level = context.user_level;
        userData.init_page = context.code_name;
      }
      //console.log('find userData', userData);
      done(null,userData);
    })
  },
  findCompanyUser : function(userData, done) {
    var findUserQuery = 'select company_id, company_name, user_level, code_name from company_user_mst '
                      + 'left join code_mst on code_id = \'C\' and code_pid = \'init.page\' '
                      + 'where 1=1 '
                      + 'and company_id = \'' + userData.id + '\' '
                      + 'and password = \'' + userData.pwd + '\''

    //console.log(findUserQuery);
    var context = {};
    pool.query(findUserQuery, (err,results) => {
      if (err) { done(err); return false; }
      if (results.rowCount === 0) {
        userData = null;
      } else {
        context = results.rows[0];
        userData.id = context.company_id;
        userData.user_name = context.company_name;
        userData.category = 'C';
        userData.user_level = context.user_level;
        userData.init_page = context.code_name;
      }
      //console.log('find userData', userData);
      done(null,userData);
    })
  },
  getUserList : function(param, callBackFunc) {
    var userListQuery = 'with users as ( '
                        + 'select user_id, user_name, coalesce(get_codenm(\'sector\',sector),\'-\') as sector, coalesce(get_codenm(\'major\',major1),\'-\') as major1, coalesce(get_codenm(\'major\',major2),\'-\') as major2, '
                        + 'coalesce(get_codenm(\'user.type\',user_type),\'-\') as user_type, coalesce(to_char(birthday,\'yyyy-mm-dd\'),\'-\') as birthday, '
                        + 'coalesce(get_codenm(\'user.sex\',sex),\'-\') as sex, phone_no, email, home_address '
                        + 'from user_mst '
                        + 'where 1=1 '
                        + 'and user_level > \'1\'';
    var userListQuery2 = '), projects as ( ' 
                        + 'select a.user_id, max(coalesce(end_date,to_date(\'29991231\',\'yyyymmdd\')) - date \'1900-01-01\' + user_project_seq ) as end_date from user_project_detail a '
                        + 'inner join users b on a.user_id = b.user_id group by a.user_id '
                        + '), list as ( '
                        + 'select a.project_name, to_char(a.end_date,\'yyyy-mm-dd\') as end_date, a.user_id from user_project_detail a '
                        + 'inner join projects b on a.user_id = b.user_id and (coalesce(a.end_date,to_date(\'29991231\',\'yyyymmdd\')) - date \'1900-01-01\' + a.user_project_seq) = b.end_date '
                        + ') '						 
                        + 'select coalesce(b.project_name,\'-\') as project_name, coalesce(b.end_date,\'-\') as end_date, a.* from users a '
                        + 'left outer join list b on a.user_id = b.user_id '
                        + 'order by a.user_name asc ';
    if (!util.isEmpty(param.userName)) { userListQuery = userListQuery + 'and user_name like \'%' + param.userName + '%\' '; }
    if (!util.isEmpty(param.sector)) { userListQuery = userListQuery + 'and sector = \'' + param.sector + '\' '; }   
    if (!util.isEmpty(param.major)) { userListQuery = userListQuery + 'and (major1 = \'' + param.major + '\' or major2 = \'' + param.major + '\') ' }
    if (!util.isEmpty(param.userType)) { userListQuery = userListQuery + 'and user_type = \'' + param.userType + '\' '; }
    userListQuery = userListQuery + userListQuery2;
    //console.log(userListQuery);
    var context = {};
    pool.query(userListQuery, (err,results) => {
      if (err) {
        context = null;
        throw err;
      } else {
        context = results.rows;
      }
      callBackFunc(context);
    })
  },
  getUser : function(userId, callBackFunc) {
    var userQuery = 'select user_id, user_name, sector, sector_detail, major1, major2, user_type, to_char(birthday,\'yyyy-mm-dd\') as birthday, sex, phone_no, fax_no, email, home_address, to_char(work_from,\'yyyy-mm-dd\') as work_from, university from user_mst '
                    + 'where 1=1 ';
    if (!util.isEmpty(userId)) { userQuery = userQuery + 'and user_id = \'' + userId + '\' '; }
    // console.log(userQuery);
    var context = {};
    pool.query(userQuery, (err,results) => {
      if (err) {
        context = null;
        throw err;
      } else {
        context = results.rows[0];
      }
      callBackFunc(context);
    })
  },
  setUser : function(query, callBackFunc) {
    var userQuery = 'UPDATE user_mst '
                  + 'SET user_name = ' + '\'' + query.userName + '\', '
                  + ' sector = ' + '\'' + query.sector + '\', '
                  + ' sector_detail = ' + '\'' + query.sectorDetail + '\', '
                  + ' major1 = ' + '\'' + query.major1 + '\', '
                  + ' major2 = ' + '\'' + query.major2 + '\', '
                  + ' user_type = ' + '\'' + query.userType + '\', '
                  + ' birthday = ' + 'to_date(\'' + query.birthday + '\',\'yyyy-mm-dd\'), '
                  + ' sex = ' + '\'' + query.sex + '\', '
                  + ' phone_no = ' + '\'' + query.phoneNo + '\', '
                  + ' university = ' + '\'' + query.university + '\', '
                  + ' fax_no =  ' + '\'' + query.faxNo + '\', '
                  + ' email = ' + '\'' + query.email + '\', '
                  + ' home_address = ' + '\'' + query.homeAddress + '\', '
                  + ' work_from = ' + 'to_date(\'' + query.workFrom + '\',\'yyyy-mm-dd\') '
                  + ' WHERE user_id = \'' + query.userId + '\'';

    pool.query(userQuery, (err,results) => {
      if(err) throw err;
      callBackFunc();
    })
  },
  newUser : function(query, callBackFunc) {
    var userQuery = 'INSERT INTO user_mst ('
                  + '	user_id, password, user_name, user_type, sex, sector, sector_detail, phone_no, email, home_address, fax_no, reg_dt, updt_dt, work_from, company_name, university, major1, major2, birthday) '
                  + 'VALUES ('
                  + 'get_worker_no(), '
                  + '\'1234\', '
                  + '\'' + query.userName + '\', '
                  + '\'' + query.userType + '\', '
                  + '\'' + query.sex + '\', '
                  + '\'' + query.sector + '\', '
                  + '\'' + query.sectorDetail + '\', '
                  + '\'' + query.phoneNo + '\', '
                  + '\'' + query.email + '\', '
                  + '\'' + query.homeAddress + '\', '
                  + '\'' + query.faxNo + '\', '
                  + 'now(), '                                                     // reg_dt
                  + 'null, '                                                     // updt_dt
                  + 'to_date(\'' + query.workFrom + '\',\'yyyy-mm-dd\'), '
                  + '\'' + query.companyName + '\', '
                  + '\'' + query.university + '\', '
                  + '\'' + query.major1 + '\', '
                  + '\'' + query.major2 + '\', '
                  + 'to_date(\'' + query.birthday + '\',\'yyyy-mm-dd\') '
                  + ')';

    pool.query(userQuery, (err,results) => {
      if(err) throw err;
      callBackFunc();
    })
  },
  getUserProject : function(userId, callBackFunc) {
    var userProjectQuery = 'select user_project_seq, user_id, project_name, to_char(start_date,\'yyyy-mm-dd\') as start_date, to_char(end_date,\'yyyy-mm-dd\') as end_date, role from user_project_detail '
                        + 'where 1=1 '
                        + 'and user_id = \'' + userId + '\' ';    
    var context = {};
    pool.query(userProjectQuery, (err,results) => {
      if (err) {
        context = null;
        throw err;
      } else {
        console.log(results.rows);
        context = {results : results.rows, user_id : userId };
      }
      callBackFunc(context);
    })
  },
  setUserProject : function(query, callBackFunc) {
    var start_date = (query.start_date != '')? query.start_date:null;
    var end_date = (query.end_date != '')? query.end_date:null;
    var userQuery = 'INSERT INTO USER_PROJECT_DETAIL (' 
                  + '  user_project_seq ' 
                  + ', user_id'
                  + ', project_name'
                  + ', start_date'
                  + ', end_date'
                  + ', role'
                  + ') VALUES ('
                  + ' nextval(\'seq_user_project_detail\')'
                  + ', \'' + query.user_id + '\''
                  + ', \'' + query.project_name + '\'';
    if (query.start_date != '') {
      userQuery = userQuery + ', to_date(\'' + start_date + '\',\'yyyy-mm-dd\')';
    } else {
      userQuery = userQuery + ', to_date(null,\'yyyy-mm-dd\')';
    }

    if (query.end_date != '') {
      userQuery = userQuery + ', to_date(\'' + end_date + '\',\'yyyy-mm-dd\')';
    } else {
      userQuery = userQuery + ', to_date(null,\'yyyy-mm-dd\')';
    }

    userQuery = userQuery + ', \'' + query.role + '\''
                  + ')'
                  ;
    console.log(userQuery);
    pool.query(userQuery, (err,results) => {
      if(err) throw err;
      callBackFunc();
    })
  },
  deleteUserProject : function(user_project_seq, callBackFunc) {
    var userQuery = 'DELETE FROM USER_PROJECT_DETAIL WHERE user_project_seq = ' + user_project_seq ;
    console.log(userQuery);
    pool.query(userQuery, (err,results) => {
      if(err) throw err;
      callBackFunc();
    })
  }
}
module.exports = User;