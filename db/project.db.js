var {Pool, Client} = require('pg');
var pool = new Pool({
    connectionString: process.env.DATABASE_URL
});
var util = require('../lib/util');

var ProjectQueries = {
  getProjects : function(query, callBackFunc) {
    var projectName = query.projectName;
    var projectEndDateFrom = query.projectEndDateFrom;
    var projectEndDateTo = query.projectEndDateTo;
    var projectsQuery = 'select project_no, project_name, perform_company, customer_company, project_leader, to_char(start_date,\'yyyy-mm-dd\') as start_date, '
                      + 'to_char(end_date,\'yyyy-mm-dd\') as end_date, location from project_mst '
                      + 'where 1=1 ';
    if (!util.isEmpty(projectName)) { projectsQuery = projectsQuery + 'and upper(trim(project_name)) like \'%' + util.trim(projectName).toUpperCase() + '%\' '; }
    if (!util.isEmpty(projectEndDateFrom)) { projectsQuery = projectsQuery + 'and end_date >= to_date(\'' + projectEndDateFrom + '\',\'yyyy-mm-dd\') ' }
    if (!util.isEmpty(projectEndDateTo)) { projectsQuery = projectsQuery + 'and end_date <= to_date(\'' + projectEndDateTo + '\',\'yyyy-mm-dd\') '; }

    pool.query(projectsQuery, (err,results) => {
      callBackFunc(results);
    })
  },
  getProject : function(query, callBackFunc) {
    var projectQuery = 'select project_no, project_name, perform_company, customer_company, project_leader, to_char(start_date,\'yyyy-mm-dd\') as start_date, '
                     + 'to_char(end_date,\'yyyy-mm-dd\') as end_date, location, project_description from project_mst '
                     + 'where project_no = ' + query.projectNo;

    pool.query(projectQuery, (err,results) => {
      console.log(results);
      callBackFunc(results.rows[0]);
    })
  },
  setProject : function(query, callBackFunc) {
    var projectQuery = 'UPDATE project_mst '
                     + 'SET perform_company = ' + '\'' + query.performCompany + '\', '                // perform_company
                     + ' customer_company = ' + '\'' + query.customerCompany + '\', '                 // customer_company
                     + ' project_leader = ' + '\'' + query.leader + '\', '                            // project_leader
                     + ' start_date = ' + 'to_date(\'' + query.startDate + '\',\'yyyy-mm-dd\'), '     // start_date
                     + ' end_date = ' + 'to_date(\'' + query.endDate + '\',\'yyyy-mm-dd\'), '         // end_date
                     + ' project_description = ' + '\'' + query.description + '\', '                  // project_description
                     + ' location = ' + '\'' + query.location + '\', '                                // location
                     + ' updt_dt = now(), '                                                           // updt_dt
                     + ' project_name = ' + '\'' + query.name + '\''                                  // project_name
                     + ' WHERE project_no = ' + query.no;

    pool.query(projectQuery, (err,results) => {
      if(err) throw err;
      callBackFunc();
    })
  },
  newProject : function(query, callBackFunc) {
    var projectQuery = 'INSERT INTO project_mst ('
                     + 'project_no, perform_company, customer_company, project_leader, start_date, end_date, project_description, location, reg_dt, updt_dt, project_name) '
                     + 'VALUES ('
                     + 'get_project_no(), '                                          // project_no
                     + '\'' + query.performCompany + '\', '                       // perform_company
                     + '\'' + query.customerCompany + '\', '                      // customer_company
                     + '\'' + query.leader + '\', '                       // project_leader
                     + 'to_date(\'' + query.startDate + '\',\'yyyy-mm-dd\'), '   // start_date
                     + 'to_date(\'' + query.endDate + '\',\'yyyy-mm-dd\'), '     // end_date
                     + '\'' + query.description + '\', '                  // project_description
                     + '\'' + query.location + '\', '                             // location
                     + 'now(), '                                                     // reg_dt
                     + 'null, '                                                     // updt_dt
                     + '\'' + query.name + '\''                            // project_name
                     + ')';
                     console.log(projectQuery);
    pool.query(projectQuery, (err,results) => {
      if(err) throw err;
      callBackFunc();
    })
  }
}

module.exports = ProjectQueries;