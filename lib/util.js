var iroom = {
    isEmpty : function(value) {
        if (value == 'null' || value == '' || value == null || value == undefined || (value != null && typeof value == 'object' && !Object.keys(value).length)) {
            return true
        } else {
            return false
        }
    },
    
    trim : function (stringToTrim) {
        return stringToTrim.replace(/^\s+|\s+$/g,"");
    }
}

module.exports = iroom;
