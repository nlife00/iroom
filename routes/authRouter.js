module.exports = function(passport) {
  var express = require('express');
  var router = express.Router();
  
  router.get('/logout', function(req, res) {
    req.logout();
    req.session.save(function() {
      res.redirect('/');
    })
  })
  
  router.get('/', function(req, res) {
    if (req.user) {
      res.render('common/layout', {initPage : req.user.init_page});
    } else { 
      res.render('common/login');
    }
  })
  
  router.post('/login',
    passport.authenticate('local',
      {
        successRedirect: '/',
        failureRedirect: '/'
      }
    )
  );
  
  return router;
};